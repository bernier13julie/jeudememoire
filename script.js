var motifsCartes = [1, 1, 2, 2, 3, 3, 4, 4, 5, 5, 6, 6, 7, 7, 8, 8, 9, 9, 10, 10];

var etatsCartes = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];

var cartesRetournees = [];
var nbPairesTrouvees = 0;
var imgCartes = document.getElementById("jeu").getElementsByTagName("img");
for (var i = 0; i < imgCartes.length; i++) {
    imgCartes[i].noCarte = i;
    imgCartes[i].onclick = function() {
        controleJeu(this.noCarte);
    }
}
initialiseJeu();

function countdown() {
    var secs = 60 * 5;

    function tick() {
        var counter = document.getElementById("counter");
        secs--;
        counter.innerHTML = "0:" + (secs < 10 ? "0" : "") + String(secs);
        if (secs > 0) {
            setTimeout(tick, 1000);
        } else {
            alert("Game over");
        }
    }
    tick();
}
countdown(60 * 5);

function majAffichage(noCarte) {
    switch (etatsCartes[noCarte]) {
        case 0:
            imgCartes[noCarte].src = "fondcarte.png";
            break;
        case 1:
            imgCartes[noCarte].src = "carte" + motifsCartes[noCarte] + ".png";
            break;
        case -1:
            imgCartes[noCarte].style.visibility = "hidden";
            break;
    }
}

function rejouer() {
    alert("Bravo !");


}

function initialiseJeu() {
    for (var position = motifsCartes.length - 1; position >= 1; position--) {
        var hasard = Math.floor(Math.random() * (position + 1));
        var sauve = motifsCartes[position];
        motifsCartes[position] = motifsCartes[hasard];
        motifsCartes[hasard] = sauve;
    }
}

function controleJeu(noCarte) {
    if (cartesRetournees.length < 2) {
        if (etatsCartes[noCarte] == 0) {
            etatsCartes[noCarte] = 1;
            cartesRetournees.push(noCarte);
            majAffichage(noCarte);
        }
        if (cartesRetournees.length == 2) {
            var nouveauEtat = 0;
            if (motifsCartes[cartesRetournees[0]] == motifsCartes[cartesRetournees[1]]) {
                nouveauEtat = -1;
                nbPairesTrouvees++;
            }

            etatsCartes[cartesRetournees[0]] = nouveauEtat;
            etatsCartes[cartesRetournees[1]] = nouveauEtat;

            setTimeout(function() {
                majAffichage(cartesRetournees[0]);
                majAffichage(cartesRetournees[1]);
                cartesRetournees = [];
                if (nbPairesTrouvees == 10) {
                    rejouer();

                }
            }, 500);
        }

    }
}